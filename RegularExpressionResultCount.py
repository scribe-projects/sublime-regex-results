import sublime
import sublime_plugin
import os
import sys
import re

if sys.version_info > (3, 0):
	from urllib.request import urlopen
	from html.parser import HTMLParser
	from html.entities import name2codepoint
else:
	from urllib2 import urlopen
	from HTMLParser import HTMLParser
	from htmlentitydefs import name2codepoint

boilerplateCode ="""===Online Resources, Dead Links Check, and Digital Hub Stats===

Regular Expressions Resource:
https://scribenet.com/wfdw/docs/resources/regex-resource.html#anchor-quotation-marks-parentheses-and-brackets

Regular Expressions Resource Supplement:
https://scribenet.com/wfdw/docs/resources/regex-resource-supplement.html

Does processing the file to ePub format and reviewing it in Kindle Previewer reveal any dead links?

Does reviewing the ePub in the Ace by Daisy App reveal any head order errors?

Review Stats and Conversion Alerts in the Digital Hub for special characters and alerts/warnings.
"""

boilerplateOCRCode ="""===Online OCR Resources===

OCR Modification:
https://scribenet.com/wfdw/docs/procedures/ocr-modification.html

OCR Standards:
https://scribenet.com/wfdw/docs/specifications/ocr-standards.html

Review Stats and Conversion Alerts in the Digital Hub for special characters and alerts/warnings.
"""

class MyHTMLParser(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self)
		self.recording = 0 
		self.data = []
		self.thisCode = ""
	def handle_starttag(self, tag, attrs):
		if tag == 'code' or tag == 'h3' or tag == 'h4':
			self.thisCode = ""
			for name, value in attrs:
				self.recording = 1

	def handle_endtag(self, tag):
		if tag == 'h3' or tag == 'h4':
			self.recording -=1
			self.data.append({'text': self.thisCode, 'group': []})
		if tag == 'code':
			self.recording -=1
			if self.thisCode:
				self.data[-1]['group'].append({'text': self.thisCode})

	def handle_data(self, data):
		if self.recording:
			self.thisCode += data

	def handle_entityref(self, name):
		if self.recording:
			self.thisCode += chr(name2codepoint[name])

class PromptRegularExpressionResultCountCommand(sublime_plugin.WindowCommand):
	selfProcessPage = ""
	selfType = ""

	def run(self, processPage, type):
		self.processPage = processPage
		self.type = type
		packagePath = os.path.join(sublime.packages_path(), "User", "scribe-regex-results-custom")
		self.files = os.listdir(packagePath)
		self.window.show_quick_panel(self.files, self.on_done)
		pass

	def on_done(self, index):
		if index == -1:
			return
			
		try:
			packagePath = os.path.join(sublime.packages_path(), "User", "scribe-regex-results-custom")
			fullPath = os.path.join(packagePath, self.files[index])
			if self.window.active_view():
				self.window.active_view().run_command("regular_expression_result_count", {"processPage" : self.processPage, "type" : self.type, "filePath" : fullPath} )
		except ValueError:
			pass

class RegularExpressionResultCountCommand(sublime_plugin.TextCommand):
	def run(self, edit, processPage, type, filePath=""):
		testView = self.view
		resultsView = self.view.window().new_file()

		self.assignSyntaxIfPossible(resultsView)

		usedBoilerplate = ""
		if type == "standard_regex":
			usedBoilerplate = boilerplateCode
		elif type == "ocr":
			usedBoilerplate = boilerplateOCRCode

		resultsString = "Results\n\n" + usedBoilerplate + "\n\n"

		if type == "file":
			fileLines = open(filePath, encoding="utf8").readlines()
			for l in fileLines:
				if l.startswith('Find: '):
					pattern = l[6:].strip()
					patternString = pattern + "\n"
					try:
						values = testView.find_all(pattern)
						if len(values) == 0:
							continue
						patternString += str(len(values)) + "\n"
					except Exception as e:
						exceptText = str(e)
						patternString += "Could not complete search: " + exceptText + "\n"
					resultsString += patternString
				else:
					resultsString += l
		else:
			html = urlopen(processPage).read().decode('utf-8')
			parser = MyHTMLParser()
			parser.feed(html)

			for head in parser.data:
				groupString = "===" + head['text'] + "===\n"
				includeGroup = False

				for pattern in head['group']:
					#print(pattern)
					patternString = pattern['text'] + "\n"

					try:
						values = testView.find_all(pattern['text'])
						if len(values) == 0:
							continue
						patternString += str(len(values)) + "\n"
					except Exception as e:
						exceptText = str(e)
						patternString += "Could not complete search: " + exceptText + "\n"

					includeGroup = True
					patternString += "\n"
					groupString += patternString

				if includeGroup == True:
					resultsString += groupString

		resultsView.run_command("insert", {"characters": resultsString})

	def assignSyntaxIfPossible(self, file):
		#Assign syntax using the API calls for different versions of Sublime.
		try:
			file.assign_syntax("scope:source.regexp")
			return
		except:
			pass

		try:
			file.set_syntax_file("Packages/Regular Expressions/RegExp.tmLanguage")
			return
		except:
			pass
