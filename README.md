# Sublime Text: Regular Expression Checks
Runs all the regular expressions on various Scribe documentation pages. Lists any expressions that had at least 1 result in a new file for further investigation.

## Sublime Text: Regular Expression Result Counts
Runs all the regular expressions on the Scribe Regex Resource documentation page (https://scribenet.com/wfdw/docs/resources/regex-resource.html).

## Sublime Text: OCR Expression Result Counts
Runs all the regular expressions on the Scribe OCR Modification documentation page (https://scribenet.com/wfdw/docs/procedures/ocr-modification.html).

## Sublime Text: Regular Expression Results from File
Runs all the regular expressions from a text configuration file. To create a configuration file, create a txt file with all your regular expression searches on their own line, preceded by the text "Find: ". Place the text file into the Sublime folder structure under Packages/User/scribe-regex-results-custom. When running the tool, any line that starts "Find: " will be evaluated as a regular expression in your current file.